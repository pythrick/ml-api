## Requirements

- Machine Learning
- The basics of web/API development
- HTTP, Json, Request, Response

## Topics

- Por que fazer uma web API
- Sobre FastAPI
- Estrutura basica de uma aplicação com FastAPI
- Documentação automática
- Validação de dados com Tipagem
- Async/Await
- FastAPI com ML
- Performance
- Docker e Dockerfile
- Opções de Deploy

## Content

“Você fala para o computador qual é o seu objetivo e ele aprende sozinho como atingir”
“E ninguém sabe exatamente o que está sendo feito”
“Ele é um sistema que transforma a si mesmo”
“Há poucas pessoas nessas companhias que entendem o que está acontecendo”
“Então, como humanos, quase perdermos o controle sobre esses sistemas”
“Eles tem mais controle sobre nós, do que nós sobre eles”

- Opções de Deploy:
  K8s, Docker Swarm, AWS, GCM, Azure, RaspberryPi

## References

https://towardsdatascience.com/build-a-fully-production-ready-machine-learning-app-with-python-django-react-and-docker-c4d938c251e5
https://www.analyticsvidhya.com/blog/2020/04/how-to-deploy-machine-learning-model-flask/
https://testdriven.io/blog/fastapi-streamlit/
https://www.youtube.com/watch?v=z9K5pwb0rt8
https://towardsdatascience.com/deploying-iris-classifications-with-fastapi-and-docker-7c9b83fdec3a