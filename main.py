import joblib

from typing import Dict, List, Union
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

model = joblib.load("./data/final_model.joblib")


class Iris(BaseModel):
    sepal_length: float
    sepal_width: float
    petal_length: float
    petal_width: float


@app.post("/predict")
async def predict(
    measurements: List[Iris],
) -> Dict[str, List[Dict[str, Union[str, float]]]]:
    data = [list(iris.dict().values()) for iris in measurements]
    predictions = model.predict(data)

    results = [
        {"prediction": str(p), **m.dict()} for p, m in zip(predictions, measurements)
    ]
    return {"results": results}
